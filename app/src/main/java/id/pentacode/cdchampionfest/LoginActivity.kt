package id.pentacode.cdchampionfest

import android.app.AlertDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.widget.Toast
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import dmax.dialog.SpotsDialog
import id.pentacode.cdchampionfest.Helper.SharedData
import id.pentacode.cdchampionfest.Model.Venue
import id.pentacode.cdchampionfest.SQLite.Venues
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.lang.Exception


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        SharedData.setKeyString(this, "url_webview", "https://dev.pentacode.id/pentaevent2/webview/")
        SharedData.setKeyString(this, "url_api", "https://dev.pentacode.id/pentaevent2/api/")
        val dialog: AlertDialog = SpotsDialog.Builder()
            .setContext(this)
            .setMessage("Tunggu")
            .setCancelable(false)
            .build()
        askPermission {
            Toast.makeText(this, "Permission All Check", Toast.LENGTH_SHORT)
        }.onDeclined { e ->
            //at least one permission have been declined by the user
        }



        val status = SharedData.getKeyString(this,"status")
        when(status){
            "true" ->{
                val intent = Intent(this@LoginActivity, MainActivityDrawer::class.java)
                startActivity(intent)
                finish()
            }
        }

        btn_login.setOnClickListener {
            if (edt_username.text.isEmpty()) {
                Toast.makeText(this@LoginActivity, "Email Belum Di Isi", Toast.LENGTH_SHORT).show()
            } else {
                if (edt_password.text.isEmpty()) {
                    Toast.makeText(this@LoginActivity, "Password Belum Di Isi", Toast.LENGTH_SHORT).show()
                } else {
                    dialog.show()
                    login(dialog)
                }
            }

        }




    }



    fun getMaps(){
        val venue_tb = Venues(this)
        val server = SharedData.getKeyString(this@LoginActivity, "url_api")
        venue_tb.deleteAll()
        Fuel.get(server + "venues").response { _, response, result ->
            when (result) {
                is Result.Success -> {
                    val json = JSONObject(String(response.data))
                    val status = json.getJSONObject("status")
                    val code = status.getInt("code")
                    val message = status.getString("message")
                    when (code) {
                        1 -> {
                            try {
                                val data = json.getJSONArray("venues")
                                for (i in 0 until data.length()) {
                                    val venue = data[i] as JSONObject
                                    val venues = Venue()

                                    venues.id = venue.getInt("id")
                                    venues.name = venue.getString("name")
                                    venues.address = venue.getString("address")
                                    venues.lat = venue.getDouble("latitude")
                                    venues.lng = venue.getDouble("longitude")
                                    venue_tb.populateData(venues)
                                    // arrayVenue.add(venues)
                                }
                            } catch (e: Exception) {

                            } finally {
                                //  dialog(arrayVenue, latlng)

                            }
                        }
                        else -> {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                is Result.Failure -> {
                    val err = result.error
                    Toast.makeText(this, err.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun login(dialog: AlertDialog) {
        val server = SharedData.getKeyString(this@LoginActivity, "url_api")
        val email = edt_username.text.toString()
        val password = edt_password.text.toString()
        val param = listOf("email" to email, "password" to password)
        Fuel.post(server + "login", param).response { request, response, result ->
            when (result) {
                is Result.Success -> {
                    val data = JSONObject(String(response.data))
                    val status = data.getJSONObject("status")
                    val code = status.getInt("code")
                    val message = status.getString("message")
                    when (code) {
                        1 -> {
                            getMaps()
                            val user = data.getJSONObject("user")
                            val id = user.getInt("id")
                            val email = user.getString("email")
                            SharedData.setKeyString(this@LoginActivity,"email", email)
                            SharedData.setKeyInt(this@LoginActivity,"id", id)
                            SharedData.setKeyString(this@LoginActivity,"status", "true")
                            dialog.dismiss()
                            val intent = Intent(this@LoginActivity, MainActivityDrawer::class.java)
                            startActivity(intent)
                            finish()
                        }
                        else -> {
                            Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
                            dialog.dismiss()
                        }
                    }
                }
                is Result.Failure -> {
                    val err = result.error
                    Toast.makeText(this@LoginActivity,err.toString(),Toast.LENGTH_SHORT).show()
                    dialog.dismiss()


                }
            }
        }

    }




}
