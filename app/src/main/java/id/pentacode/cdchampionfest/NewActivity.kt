package id.pentacode.cdchampionfest

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity;
import android.view.View
import android.webkit.*
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_new.*
import kotlinx.android.synthetic.main.content_new.*
import java.text.SimpleDateFormat
import java.util.*
import id.pentacode.cdchampionfest.Helper.SharedData
import android.app.DownloadManager
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.*
import android.media.ExifInterface
import android.provider.DocumentsContract
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import id.pentacode.cdchampionfest.Service.MyService
import kotlinx.android.synthetic.main.dialog_image.view.*
import java.io.*
import android.util.Base64
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import org.json.JSONObject
import java.lang.Exception
import java.net.URISyntaxException

class NewActivity : AppCompatActivity() {

    private val INPUT_FILE_REQUEST_CODE = 1
    private val FILECHOOSER_RESULTCODE = 1
    private var mUploadMessage: ValueCallback<Uri>? = null
    private var mCapturedImageURI: Uri? = null
    private var mFilePathCallback: ValueCallback<Array<Uri>>? = null
    private var mCameraPhotoPath: String? = null
    var address: String? = ""
    var titles: String? = ""
    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        titles = SharedData.getKeyString(this, "title")
        title = titles
        val url_ = SharedData.getKeyString(this, "url_webview")
        val embed_url = SharedData.getKeyString(this, "embed_url")
        val email = SharedData.getKeyString(this, "email")
        address = "$url_$embed_url?my_email=$email&button=true"
        //address = "https://facebook.com"
        fab.hide()

        when(titles){
            "Moments" ->{

                fab.show()
            }
            "Profile" ->{

                fab.show()
            }
        }

        when {
            Build.VERSION.SDK_INT >= 21 -> {
                webview.settings.mixedContentMode = 0
                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            }
            Build.VERSION.SDK_INT >= 19 ->
                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            else ->
                webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        web(address!!)

        swipeContainer.setOnRefreshListener {
            webview.reload()
        }
        fab.setOnClickListener {
           // dialogs()
            var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent!!.resolveActivity(packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                    takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath)
                } catch (ex: IOException) {
                    // Error occurred while creating the File

                }

                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.absolutePath
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile))
                } else {
                    takePictureIntent = null
                }
            }

            // Set up the intent to get an existing image
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "image/*"

            // Set up the intents for the Intent chooser
            val intentArray: Array<Intent>
            if (takePictureIntent != null) {
                intentArray = arrayOf(takePictureIntent)
            } else {
                intentArray = emptyArray()
            }

            val chooserIntent = Intent(Intent.ACTION_CHOOSER)
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)

            startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE)

        }
    }


    override fun onSaveInstanceState(outState: Bundle?) {

        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {

        super.onRestoreInstanceState(savedInstanceState)
    }


    override fun onResume() {
        //webview.reload()
        super.onResume()
    }


    override fun onStop() {
        val titles = SharedData.getKeyString(this@NewActivity, "title")
        when (titles) {
            "Attendance" -> {
                if (webview.canGoBack()) {
                    webview.loadUrl(address)
                }
            }
        }
        super.onStop()
    }


    fun web(address: String) {
        webview.run {
            settings.javaScriptEnabled = true
            settings.setSupportZoom(false)
            settings.allowFileAccess = true
            settings.allowContentAccess = true
            settings.allowFileAccessFromFileURLs = true
            settings.allowUniversalAccessFromFileURLs = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.domStorageEnabled = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            loadUrl(address)
        }

        when (titles) {
            "Instructions" -> {
                webview.run {
                    settings.setSupportZoom(true)
                    settings.displayZoomControls = true
                    settings.builtInZoomControls = true
                }
            }
            "Rundown" ->{
                webview.run {
                    settings.setSupportZoom(true)
                    settings.displayZoomControls = true
                    settings.builtInZoomControls = true
                }
            }
        }


        webview.webViewClient = object : WebViewClient() {
            @SuppressWarnings("deprecation")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url!!.startsWith("tel:")) {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                } else if (url!!.startsWith("http:") || url.startsWith("https:")) {
                    view?.loadUrl(url)
                    val uri = Uri.parse(view!!.url)
                    val titles = SharedData.getKeyString(this@NewActivity, "title")
                    when (titles) {
                        "Attendance" -> {
                            Log.e("ATTENDANCES", url.toString())
                            val email = uri.getQueryParameter("my_email")
                            val id = uri.getQueryParameter("attendance_id")
                            SharedData.setKeyString(this@NewActivity, "email_att", email)
                            SharedData.setKeyString(this@NewActivity, "id_att", id)
                            val intent = Intent(this@NewActivity, QRCodeActivity::class.java)
                            startActivity(intent)
                        }

                    }

                }
                return super.shouldOverrideUrlLoading(view, url)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    val url = request!!.url.toString()
                    if (url.startsWith("tel:")) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        intent.data = Uri.parse(url)
                        startActivity(intent)
                    } else if (url.startsWith("http:") || url.startsWith("https:")) {
                        view?.loadUrl(request!!.url.toString())
                        val uri = Uri.parse(view!!.url)
                        val titles = SharedData.getKeyString(this@NewActivity, "title")
                        when (titles) {
                            "Attendance" -> {
                                Log.e("ATTENDANCES", url.toString())
                                val email = uri.getQueryParameter("my_email")
                                val id = uri.getQueryParameter("attendance_id")
                                SharedData.setKeyString(this@NewActivity, "email_att", email)
                                SharedData.setKeyString(this@NewActivity, "id_att", id)
                                val intent = Intent(this@NewActivity, QRCodeActivity::class.java)
                                startActivity(intent)
                            }

                        }

                    }

                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                //  Log.e("URLS====",url.toString())

                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                swipeContainer.isRefreshing = false
                super.onPageFinished(view, url)
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                Toast.makeText(this@NewActivity, "Connection Problem", Toast.LENGTH_SHORT).show()
                super.onReceivedError(view, request, error)
            }


        }

        webview.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val request = DownloadManager.Request(Uri.parse(url))
            request.allowScanningByMediaScanner()

            request.setNotificationVisibility(
                DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
            )

            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS, //Download folder
                "download"
            )                        //Name of file


            val dm = getSystemService(
                Context.DOWNLOAD_SERVICE
            ) as DownloadManager

            dm.enqueue(request)
        }

        webview.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val request = DownloadManager.Request(
                Uri.parse(url)
            )
            val filename = URLUtil.guessFileName(url, contentDisposition, mimetype)
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                filename
            )
            val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            dm.enqueue(request)
            Toast.makeText(
                applicationContext, "Downloading File", //To notify the Client that the file is being downloaded
                Toast.LENGTH_LONG
            ).show()
        }

    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES
        )
        return File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir      /* directory */
        )
    }

    private fun getPath(selectedImaeUri: Uri): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)

        val cursor = managedQuery(selectedImaeUri, projection, null, null, null)

        if (cursor != null) {
            cursor.moveToFirst()
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            return cursor.getString(columnIndex)
        }
        return selectedImaeUri.path
    }

    @Throws(URISyntaxException::class)
    fun getFilePath(context: Context, uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val envi = Environment.getExternalStorageDirectory()
                val cuk = (envi.toString()+"/"+split[1])
                return cuk
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                if ("image" == type) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                selection = "_id=?"
                selectionArgs = arrayOf(split[1])
            }
        }
        if ("content".equals(uri.getScheme(), ignoreCase = true)) {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor? = null
            try {
                cursor = context.contentResolver
                    .query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor!!.moveToFirst()) {
                    return cursor!!.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
            return uri.getPath()
        }
        return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }


    fun getRealPathFromURI(context: Context, uri: Uri): String {
        var filePath = ""
        val wholeID = DocumentsContract.getDocumentId(uri)

        // Split at colon, use second item in the array
        val id = wholeID.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

        val column = arrayOf(MediaStore.Images.Media.DATA)

        // where id is equal to
        val sel = MediaStore.Images.Media._ID + "=?"

        val cursor = context.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            column, sel, arrayOf(id), null
        )

        val columnIndex = cursor!!.getColumnIndex(column[0])

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex)
        }
        cursor.close()
        return filePath
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                super.onActivityResult(requestCode, resultCode, data)

            if (resultCode === RESULT_OK) {
                if (data == null) {
                    if (mCameraPhotoPath != null) {
                        val a = Uri.parse(mCameraPhotoPath)
                        val photoPath = getPath(a)
                     try {
                         val b = decodeFile(photoPath!!)
                         val byteArrayOutputStream = ByteArrayOutputStream()
                         b!!.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream)
                         val byteArray = byteArrayOutputStream.toByteArray()
                         val image = Base64.encodeToString(byteArray, Base64.NO_WRAP)
                         dialog_image(image,b)
                     }catch (e: Exception){
                         Toast.makeText(this@NewActivity,"error", Toast.LENGTH_SHORT).show()
                         System.exit(0)
                     }



                    }
                } else {
                    val dataString = data.dataString
                    if (dataString != null) {

                        val a = Uri.parse(dataString)
                        val photoPath = getFilePath(this@NewActivity, a)
                        try{
                                val b = decodeFile(photoPath!!)
                                val byteArrayOutputStream = ByteArrayOutputStream()
                                b!!.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream)
                                val byteArray = byteArrayOutputStream.toByteArray()
                                val image = Base64.encodeToString(byteArray, Base64.NO_WRAP)
                                dialog_image(image, b)
                        }catch (e: Exception){
                            Toast.makeText(this@NewActivity,"error", Toast.LENGTH_SHORT).show()
                            System.exit(0)
                        }

                    }
                }
            }

        }
        return

    }



    fun dialog_image(bs: String,  b: Bitmap) {

        val dialogs = LayoutInflater.from(this).inflate(R.layout.dialog_image, null)
        Glide.get(this).clearMemory()
        Glide.with(applicationContext)
            .load(b)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE).skipMemoryCache(true))
            .into(dialogs.img_uploads)

//        val storageDir = Environment.getExternalStoragePublicDirectory(
//            Environment.DIRECTORY_PICTURES
//        )
//        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
//        val shareFile = File.createTempFile(timeStamp+"SHARE_", ".jpg", storageDir)
//        val output = FileOutputStream(shareFile)

        var alertbox = android.app.AlertDialog.Builder(this).apply {
            setView(dialogs)
            setPositiveButton("OK"){dialog, which ->
                val email = SharedData.getKeyString(this@NewActivity, "email")
                val id = SharedData.getKeyInt(this@NewActivity,"id")
                when(titles){
                    "Profile"->{

                        uploadImageProfile(bs)
                    }
                    "Moments"->{
                        uploadImageGallery(bs)
                    }
                }


            }
            setNegativeButton("BACK") { dialog, whichButton ->
                dialog.dismiss()
            }
            show()
        }
    }


    fun uploadImageProfile(bs: String){
        val url_upload = "https://dev.pentacode.id/pentaevent2/api/update_profile_picture_base64"
        val email = SharedData.getKeyString(this@NewActivity, "email")
        AndroidNetworking.post(url_upload)
            .addBodyParameter("my_email", email)
            .addBodyParameter("image", bs)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    // do anything with response
                    webview.reload()
                }

                override fun onError(error: ANError) {
                    // handle error
                    Toast.makeText(this@NewActivity,"Sedang Bermasalah Coba Lagi", Toast.LENGTH_SHORT).show()
                }
            })
    }

    fun uploadImageGallery(bs: String){
        val url_upload = "https://dev.pentacode.id/pentaevent2/api/upload_image_base64"
        val id = SharedData.getKeyInt(this@NewActivity,"id")
        AndroidNetworking.post(url_upload)
            .addBodyParameter("uploader_id", id.toString())
            .addBodyParameter("image", bs)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    // do anything with response
                    webview.reload()
                }

                override fun onError(error: ANError) {
                    // handle error
                    Toast.makeText(this@NewActivity,"Sedang Bermasalah Coba Lagi", Toast.LENGTH_SHORT).show()
                }
            })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val titles = SharedData.getKeyString(this, "title")
        when (titles) {
            "Profile" -> {
                menuInflater.inflate(R.menu.logout, menu)
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_logout -> {
                SharedData.setKeyString(this@NewActivity, "status", "false")
                val intents = Intent(this@NewActivity, MyService::class.java)
                stopService(intents)
                finish()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun decodeFile(filePath: String): Bitmap? {

        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(filePath, o)

        // The new size we want to scale to
        val REQUIRED_SIZE = 1024

        // Find the correct scale value. It should be the power of 2.
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }

        // Decode with inSampleSize
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        var image = BitmapFactory.decodeFile(filePath, o2)

        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)
            val exifOrientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )

            var rotate = 0
            when (exifOrientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90

                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180

                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
            }

            if (rotate != 0) {
                val w = image.width
                val h = image.height

                // Setting pre rotate
                val mtx = Matrix()
                mtx.preRotate(rotate.toFloat())

                // Rotating Bitmap & convert to ARGB_8888, required by tess
                image = Bitmap.createBitmap(image, 0, 0, w, h, mtx, false)

            }
        } catch (e: IOException) {
            return null
        }

        return image.copy(Bitmap.Config.ARGB_8888, true)
    }
}

