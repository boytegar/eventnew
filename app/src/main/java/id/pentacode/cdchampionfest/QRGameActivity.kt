package id.pentacode.cdchampionfest

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import id.pentacode.cdchampionfest.Helper.SharedData
import kotlinx.android.synthetic.main.activity_qrgame.*
import kotlinx.android.synthetic.main.content_qrgame.*
import org.json.JSONObject
import java.util.*

class QRGameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrgame)
        setSupportActionBar(toolbar)

        val formats = Arrays.asList(BarcodeFormat.QR_CODE)
        barcode_view1.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        barcode_view1.initializeFromIntent(intent)
        barcode_view1.decodeContinuous(callback)
        barcode_view1.setStatusText("Pastikan Posisi Barcode Pas")
    }
    override fun onResume() {
        super.onResume()
        barcode_view1.resume()
    }

    override fun onPause() {
        super.onPause()
        barcode_view1.pause()
    }


    private val callback = object : BarcodeCallback {
        var lastText: String? = null
        override fun barcodeResult(result: BarcodeResult) {
            lastText = result.text
            // barcode_view.setStatusText(result.text)
            if (result.text == null || result.text == lastText) {
                barcode_view1.pause()
                //dialog(lastText.toString())
                postGame(lastText.toString())
            }
        }

        override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    }
    fun dialog(message: String, code: Int) {
        //  val dialogs = LayoutInflater.from(this@DetailPengirimanActivity).inflate(R.layout.dialog_reason, null)
        var alertbox = android.app.AlertDialog.Builder(this).apply {
            setTitle(message)
            setPositiveButton("OK") { dialog, whichButton ->
                //pass
                when(code){
                    1->{
                        SharedData.setKeyString(this@QRGameActivity, "title", "Games")
                        //SharedData.setKeyString(this@QRGameActivity, "embed_url", "games")
                        val intent = Intent(this@QRGameActivity, NewActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    else->{
                        finish()
                    }
                }

            }
            show()
        }
    }

    fun postGame(word: String){
        val server = SharedData.getKeyString(this@QRGameActivity,"url_api")
        val id = SharedData.getKeyInt(this@QRGameActivity,"id")
        val param = listOf("word" to word)
        Fuel.post(server!!+"games/$id",param).response{ request, response, result ->
            when (result) {
                is Result.Success -> {
                    val data = JSONObject(String(response.data))
                    val status = data.getJSONObject("status")
                    val code = status.getInt("code")
                    val message = status.getString("message")
                    when(code){
                        1->{
                            //finish()
                            dialog(message, code)
                            //   Toast.makeText(this@QRCodeActivity, message, Toast.LENGTH_SHORT).show()
                        }
                        else->{
                            dialog(message, code)
                            // Toast.makeText(this@QRCodeActivity, message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                is Result.Failure ->{
                    //if error
                }
            }


        }
    }
}
