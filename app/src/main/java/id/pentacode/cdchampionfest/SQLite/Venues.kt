package id.pentacode.cdchampionfest.SQLite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import id.pentacode.cdchampionfest.Helper.SQLiteHelper
import id.pentacode.cdchampionfest.Model.Venue

class Venues(context: Context) {

    val sqlhelper = SQLiteHelper(context)

    fun populateData(venue: Venue) {
        val db = sqlhelper.writableDatabase
        var values = ContentValues()
        values.put("id", venue.id)
        values.put("name", venue.name)
        values.put("address", venue.address)
        values.put("latitude", venue.lat)
        values.put("longitude", venue.lng)
        db.insert("tb_venue", null, values)
        db.close()
        Log.v("@@@WWe ", " Record Inserted Sucessfully")
    }

    fun deleteAll() {
        val db = sqlhelper.readableDatabase
        db.execSQL("delete from tb_venue")
    }

    fun getAllMaps(): List<Venue> {
        val db = sqlhelper.writableDatabase
        val list = ArrayList<Venue>()
        val cusrsor: Cursor
        cusrsor = db.rawQuery("SELECT * FROM tb_venue", null)
        if (cusrsor != null) {
            if (cusrsor.count > 0) {
                cusrsor.moveToFirst()
                do {
                    val id = cusrsor.getInt(cusrsor.getColumnIndex("id"))
                    val name = cusrsor.getString(cusrsor.getColumnIndex("name"))
                    val address = cusrsor.getString(cusrsor.getColumnIndex("address"))
                    val lat = cusrsor.getDouble(cusrsor.getColumnIndex("latitude"))
                    val lng = cusrsor.getDouble(cusrsor.getColumnIndex("longitude"))
                    val venue =Venue()
                    venue.id = id
                    venue.name = name
                    venue.address = address
                    venue.lat = lat
                    venue.lng = lng
                    list.add(venue)
                } while (cusrsor.moveToNext())
            }
        }
        return list
    }



//    fun deleteUser(users: Users) {
//        val db = this.writableDatabase
//        var values = ContentValues()
//        values.put("userID", users.userID)
//        values.put("userName", users.userName)
//        values.put("userAge", users.userAge)
//        val retVal = db.delete("USER", "userID = " + users.userID, null)
//        if (retVal >= 1) {
//            Log.v("@@@WWe", " Record deleted")
//        } else {
//            Log.v("@@@WWe", " Not deleted")
//        }
//        db.close()
//
//    }
}