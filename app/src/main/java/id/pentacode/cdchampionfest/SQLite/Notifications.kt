package id.pentacode.cdchampionfest.SQLite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import id.pentacode.cdchampionfest.Helper.SQLiteHelper
import id.pentacode.cdchampionfest.Model.Notification

class Notifications(context: Context){

    val sqlhelper = SQLiteHelper(context)

    fun populateData(notif: Notification) {
        val db = sqlhelper.writableDatabase
        var values = ContentValues()
        values.put("id", notif.id)
        values.put("name", notif.name)
        values.put("description", notif.desc)
        values.put("type", notif.type)
        values.put("time", notif.time)
        values.put("status", notif.status)
        db.insert("tb_notif", null, values)
        db.close()
        Log.v("@@@WWe ", " Record Inserted Sucessfully")
    }


    fun getNotif(): List<Notification> {
        val db = sqlhelper.writableDatabase
        val list = ArrayList<Notification>()
        val cusrsor: Cursor
        cusrsor = db.rawQuery("SELECT * FROM tb_notif", null)
        if (cusrsor != null) {
            if (cusrsor.count > 0) {
                cusrsor.moveToFirst()
                do {
                    val id = cusrsor.getInt(cusrsor.getColumnIndex("id"))
                    val name = cusrsor.getString(cusrsor.getColumnIndex("name"))
                    val descript = cusrsor.getString(cusrsor.getColumnIndex("description"))
                    val type = cusrsor.getString(cusrsor.getColumnIndex("type"))
                    val time = cusrsor.getString(cusrsor.getColumnIndex("time"))
                    val notifi = Notification()
                    notifi.id = id
                    notifi.name = name
                    notifi.desc = descript
                    notifi.type = type
                    notifi.time = time
                    list.add(notifi)
                } while (cusrsor.moveToNext())
            }
        }
        return list
    }

    fun getLastId(): Int{
        val db = sqlhelper.readableDatabase
        var lastId: Int = 0
        val query = "SELECT max(id) from tb_notif"
        val c = db.rawQuery(query, null)
        if (c != null && c!!.moveToFirst()) {
            lastId = c!!.getInt(0) //The 0 is the column index, we only have 1 column, so the index is 0
        }
        return lastId
    }

    fun getNotifications(status: Int): List<Notification>{
        val db = sqlhelper.writableDatabase
        val list = ArrayList<Notification>()
        val cusrsor: Cursor
        cusrsor = db.rawQuery("SELECT * FROM tb_notif WHERE status=$status ", null)
        if (cusrsor != null) {
            if (cusrsor.count > 0) {
                cusrsor.moveToFirst()
                do {
                    val id = cusrsor.getInt(cusrsor.getColumnIndex("id"))
                    val name = cusrsor.getString(cusrsor.getColumnIndex("name"))
                    val descript = cusrsor.getString(cusrsor.getColumnIndex("description"))
                    val type = cusrsor.getString(cusrsor.getColumnIndex("type"))
                    val time = cusrsor.getString(cusrsor.getColumnIndex("time"))
                    val status = cusrsor.getInt(cusrsor.getColumnIndex("status"))
                    val notifi = Notification()
                    notifi.id = id
                    notifi.name = name
                    notifi.desc = descript
                    notifi.type = type
                    notifi.time = time
                    notifi.status = status
                    list.add(notifi)
                } while (cusrsor.moveToNext())
            }
        }
        return list
    }

    fun updateNotif(id: Int) {
        val db = sqlhelper.writableDatabase
        var values = ContentValues()
        values.put("status", 0)

        val retVal = db.update("tb_notif", values, "id = $id", null)
        if (retVal >= 1) {
            Log.v("@@@WWe", " Record updated")
        } else {
            Log.v("@@@WWe", " Not updated")
        }
        db.close()

    }

//    fun deleteUser(users: Users) {
//        val db = this.writableDatabase
//        var values = ContentValues()
//        values.put("userID", users.userID)
//        values.put("userName", users.userName)
//        values.put("userAge", users.userAge)
//        val retVal = db.delete("USER", "userID = " + users.userID, null)
//        if (retVal >= 1) {
//            Log.v("@@@WWe", " Record deleted")
//        } else {
//            Log.v("@@@WWe", " Not deleted")
//        }
//        db.close()
//
//    }
}