package id.pentacode.cdchampionfest.Model

class Venue() {

    var id: Int? = null
    var name: String? = null
    var address: String? = null
    var lng: Double? = null
    var lat: Double? = null


    constructor(_id: Int, _name: String, _address: String, _lng: Double, _lat: Double): this(){
        id = _id
        name = _name
        address = _address
        lng = _lng
        lat = _lat

    }
}