package id.pentacode.cdchampionfest.Model

class Game() {

    var id: Int? = null
    var name: String? = null
    var cover: String? = null
    var description: String? = null
    var type: String? = null
    var view: String? = null
    var scan_status: Boolean? = null
    var webview_status: Boolean? = null
    var next_url: String? = null

    constructor(_id: Int, _name: String,_cover: String, _desc: String, _type: String, _view: String, _scan_status: Boolean, _webview_status: Boolean, _next_url: String): this(){
        id = _id
        name = _name
        cover = _cover
        description = _desc
        type = _type
        view = _view
        scan_status = _scan_status
        webview_status = _webview_status
        next_url = _next_url
    }
}