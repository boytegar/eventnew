package id.pentacode.cdchampionfest.Model

class Notification(){
    var id: Int? = null
    var name: String? = null
    var desc: String? = null
    var type: String? = null
    var time: String? = null
    var status: Int? = null

    constructor(_id: Int, _name: String, _desc: String, _type: String, _time: String, _status: Int): this(){
        this.id = _id
        this.name = _name
        this.desc = _desc
        this.type = _type
        this.time = _time
        this.status = _status
    }

}