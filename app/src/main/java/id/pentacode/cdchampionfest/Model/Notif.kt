package id.pentacode.cdchampionfest.Model

class Notif() {

    var id: Int? = null
    var title: String? = null
    var desc: String? = null


    constructor(_id: Int, _title: String, _desc: String): this(){
        id = _id
        title = _title
        desc = _desc

    }


}