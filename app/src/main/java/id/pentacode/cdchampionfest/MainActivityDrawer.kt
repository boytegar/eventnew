package id.pentacode.cdchampionfest

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.opengl.Visibility
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.javiersantos.appupdater.AppUpdater
import com.github.javiersantos.appupdater.AppUpdaterUtils
import com.github.javiersantos.appupdater.enums.AppUpdaterError
import com.github.javiersantos.appupdater.enums.UpdateFrom
import com.github.javiersantos.appupdater.objects.Update
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import id.pentacode.cdchampionfest.Adapter.ItemAdapter
import id.pentacode.cdchampionfest.Adapter.VenueAdapter
import id.pentacode.cdchampionfest.Helper.SharedData
import id.pentacode.cdchampionfest.Model.Game
import id.pentacode.cdchampionfest.Model.Venue
import id.pentacode.cdchampionfest.SQLite.Venues
import id.pentacode.cdchampionfest.Service.MyService
import id.pentacode.dfa.Helper.CheckConnection
import id.pentacode.dfa.Helper.SingleLocation
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_drawer.*
import kotlinx.android.synthetic.main.app_bar_main_activity_drawer.*
import kotlinx.android.synthetic.main.dialog_venue.*
import kotlinx.android.synthetic.main.dialog_venue.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import org.jsoup.Jsoup
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.lang.Exception

class MainActivityDrawer : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var array = ArrayList<Game>()
    var arrayVenue = ArrayList<Venue>()
    var act: Int = 0
    var urls: String? = ""
    var username: String? = ""
    var subname: String? = ""
    var name_point_personal: String? = ""
    var name_point_team: String? = ""
    var value_point_personal: Int? = 0
    var value_point_team: Int? = 0
    var click = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_drawer)
        setSupportActionBar(toolbar)
        title = "Home"

        //  web_update()


        btn_schedule.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Rundown")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "schedules")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_presence.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Attendance")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "attendances")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_feedback.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Feedbacks")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "feedbacks")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_contact.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Contacts")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "contacts")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }

        btn_gallery.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Moments")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "galleries")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_polling.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Pollings")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "pollings")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_qna.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Questions")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "questions")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_quiz.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Quizes")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "quizes")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_test.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Testimonials")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "testimonials")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_profile.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Profile")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "profile")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url_api", "profile_base64")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)

        }
        btn_instructions.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Instructions")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "slider")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)
        }
        btn_leader.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Leaderboard")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "leaderboard")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)
        }
        btn_video.setOnClickListener {
            SharedData.setKeyString(this@MainActivityDrawer, "title", "Videos")
            SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "teaser")
            val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
            startActivity(intent)
        }
        btn_venue.setOnClickListener {
            val venues = Venues(this)
            arrayVenue = venues.getAllMaps() as ArrayList<Venue>
            dialog(arrayVenue)
        }


        //loads()
     //   getDashboard()
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    fun checkVersion(){
        val curVersion = applicationContext.packageManager.getPackageInfo(this@MainActivityDrawer.packageName, 0).versionName
        Log.e("VERSION NAME", curVersion)
        val param = listOf("version" to curVersion)
        val server = SharedData.getKeyString(this@MainActivityDrawer, "url_api")
        Fuel.get(server+"check_version", param).response{request, response, result ->
            when(result){
                is Result.Success ->{
                    val data = JSONObject(String(response.data))
                    val status = data.getJSONObject("status")
                    val code = status.getInt("code")
                    Log.e("NOTIFICATION", "SERVER AKTIF")
                    when(code){
                        0->{
                            Log.e("NOTIFICATION", "VERSI LAMA")
                            Toast.makeText(this@MainActivityDrawer,"Terdapat versi terbaru mohon segera update", Toast.LENGTH_LONG).show()
                            SharedData.setKeyString(this@MainActivityDrawer,"status", "false")
                            val appPackageName = packageName // getPackageName() from Context or Activity object
                            try {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id=$appPackageName")
                                    )
                                )
                            } catch (anfe: android.content.ActivityNotFoundException) {
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                    )
                                )
                            }
                            finish()
                        }
                    }
                }
                is Result.Failure ->{
                    Log.e("FAILURE", response.toString())
                }
            }
        }
    }

    private fun web_update() {
        try {
            val curVersion = applicationContext.packageManager.getPackageInfo(this@MainActivityDrawer.packageName, 0).versionName
            var newVersion: String? = null
            val appUpdaterUtils = AppUpdaterUtils(this)
                .setUpdateFrom(UpdateFrom.GOOGLE_PLAY)
                .withListener(object : AppUpdaterUtils.UpdateListener {
                    override fun onSuccess(update: Update, isUpdateAvailable: Boolean?) {
                        Log.e("Latest Version ", update.latestVersion)
                        Log.e("Release notes", update.releaseNotes)
                        Log.e("URL", update.urlToDownload.toString())
                       // val appPackageName = packageName // getPackageName() from Context or Activity object
                        val ver = update.latestVersion
                        Log.e("CURVERSION",curVersion)
                        when{
                            (curVersion != ver) ->{
                                try {
                                    Toast.makeText(this@MainActivityDrawer,"Terdapat versi terbaru mohon segera update", Toast.LENGTH_LONG).show()
                                    SharedData.setKeyString(this@MainActivityDrawer,"status", "false")
                                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(update.urlToDownload.toString())))
                                    finish()
                                } catch (anfe: android.content.ActivityNotFoundException) {
                                    SharedData.setKeyString(this@MainActivityDrawer,"status", "false")
                                    Toast.makeText(this@MainActivityDrawer,"Terdapat versi terbaru mohon segera update", Toast.LENGTH_LONG).show()
                                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(update.urlToDownload.toString()))
                                    )
                                    finish()
                                }
                            }
                        }


                    }

                    override fun onFailed(error: AppUpdaterError) {
                        Log.e("AppUpdater Error", "Something went wrong")
                    }
                })
            appUpdaterUtils.start()


        } catch (e: Exception) {
            e.printStackTrace()
            //return false
        }

    }



    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }

    override fun onResume() {
        checkConnection()
        super.onResume()

    }

    fun checkConnection(){
        val status = SharedData.getKeyString(this,"status")
        when(status){
            "true" ->{
//
                when {
                    isNetworkConnected() -> {
                        getDashboard()
                        loads()
                        try {
                            checkVersion()
                        }catch (e: Exception){
                            Log.e("CATCH MESSAGE", e.toString())
                        }
                    }
                    else ->{
                        dialogConnection()
                    }
                }
            }
            "false" ->{
                val intent = Intent(this@MainActivityDrawer, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onStart() {
        val intents = Intent(this@MainActivityDrawer, MyService::class.java)
        startService(intents)
        super.onStart()
    }


    fun dialogConnection(){

        var alertbox = android.app.AlertDialog.Builder(this).apply {
            setMessage("Connection Problem, please check your connection")
            setCancelable(false)
            setNegativeButton("OK") { dialog, whichButton ->
                when{
                    CheckConnection.isConnected() -> {
                        checkConnection()
                        dialog.dismiss()}
                    else ->{
                        dialog.dismiss()
                        dialogConnection()
                    }
                }

            }
            show()
        }
    }

    fun getDashboard() {
        val server = SharedData.getKeyString(this@MainActivityDrawer, "url_api")
        val email = SharedData.getKeyString(this@MainActivityDrawer,"email")
        val params = listOf("my_email" to email)
        Fuel.get(server + "dashboard_data", params).response { request, response, result ->
            when (result) {
                is Result.Failure -> {
                    val err = result.error
                    Toast.makeText(this@MainActivityDrawer,err.toString(),Toast.LENGTH_SHORT).show()
                }
                is Result.Success -> {
                    try {
                        val data = JSONObject(String(response.data))
                        val status = data.getJSONObject("status")
                        val code = status.getInt("code")
                        when(code){
                            1->{
                                val alldata = data.getJSONArray("data")
                                for(i in 0 until alldata.length()){
                                    val datas = alldata[i] as JSONObject
                                     urls= datas.getString("default_user_picture")
                                     username= datas.getString("username")
                                     subname= datas.getString("username_sub")
                                     name_point_personal= datas.getString("point_personal_label")
                                     name_point_team= datas.getString("point_team_label")
                                     value_point_personal= datas.getInt("point_personal_amount")
                                     value_point_team= datas.getInt("point_team_amount")
                                }

                            }
                        }

                    } catch (e: Exception) {
                    } finally {
                        SharedData.setKeyString(this@MainActivityDrawer,"email", subname!!)
                        txt_label_personal.text = name_point_personal
                        txt_point_personal.text = value_point_personal.toString()
                        txt_team_label.text = name_point_team
                        txt_team_point.text = value_point_team.toString()
                        txt_name.text = "Welcome, "+username
                        txt_subname.text = subname
                        val options = RequestOptions()
                            .centerCrop()
                            .placeholder(R.mipmap.ic_launcher_round)
                            .error(R.mipmap.ic_launcher_round)
                        Glide.with(this).load(urls).apply(options).into(img_profile)

                    }
                }
            }
        }
    }

    fun loads() {
        act = 0
        array.clear()
        val server = SharedData.getKeyString(this@MainActivityDrawer, "url_api")
        val id = SharedData.getKeyString(this@MainActivityDrawer, "email")
        val param = listOf("my_email" to id)
        Fuel.get(server + "games", param).response { request, response, result ->
            when (result) {
                is Result.Failure -> {

                }
                is Result.Success -> {
                    try {
                        val data = JSONObject(String(response.data))
                        val status = data.getJSONObject("status")
                        val code = status.getInt("code")
                        val message = status.getString("message")
                        when (code) {
                            1 -> {
                                val games = data.getJSONArray("games")
                                for (i in 0 until games.length()) {
                                    val alldata = games[i] as JSONObject
                                    var id = alldata.getInt("id")
                                    var name = alldata.getString("name")
                                    var cover = alldata.getString("cover")
                                    var description = alldata.getString("description")
                                    var type = alldata.getString("type")
                                    var view = alldata.getString("view")
                                    var scan_status = alldata.getBoolean("scan_status")
                                    var webview_status = alldata.getBoolean("webview_status")
                                    var next_url= alldata.getString("next_url")
                                   // Log.e("STATUS", scan_status.toString()+" ----- "+webview_status)
                                    when  {
                                        (!scan_status && !webview_status) -> {
                                                    act++
                                        }

                                    }
                                    val game = Game()
                                    game.id = id
                                    game.name = name
                                    game.cover = cover
                                    game.view = view
                                    game.description = description
                                    game.type = type
                                    game.scan_status = scan_status
                                    game.webview_status = webview_status
                                    game.next_url = next_url
                                    array.add(game)
                                }
                            }
                            else -> {
                                Toast.makeText(this@MainActivityDrawer, message, Toast.LENGTH_SHORT).show()
                            }
                        }
                    } catch (e: Exception) {
                    } finally {
                        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                        val categoryAdapter = ItemAdapter(R.layout.list_game, array)
                        recycle_item.layoutManager = layoutManager
                        recycle_item.hasFixedSize()
                        recycle_item.adapter = categoryAdapter
                        txt_activities.text = ("Today Activity : " + act.toString() + "/" + array.size.toString())
                        when (array.size) {
                            0 -> {
                                guideline17.setGuidelinePercent(0.28F)
                                recycle_item.visibility = View.GONE
                                txt_activities.visibility = View.GONE
                            }
                        }
                    }

                }

            }
        }
    }

    //check gps
    fun onLocation() {
        val manager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showSettingsAlert()
        } else {
            SingleLocation.requestSingleUpdate(this, object : SingleLocation.LocationCallback {
                override fun onNewLocationAvailable(location: SingleLocation.Companion.GPSCoordinates) {
                    if (location != null) {
                        onLocationChanged(location)
                    } else {
                        Toast.makeText(this@MainActivityDrawer, "Lokasi Sedang Di Ambil", Toast.LENGTH_SHORT).show()
                        startLocationUpdates()
                    }
                }

            })
        }
    }

    private fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("GPS is not Enabled!")
        alertDialog.setMessage("Do you want to turn on GPS?")
        alertDialog.setPositiveButton("Yes") { dialog, _ ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
        alertDialog.setNegativeButton("No") { dialog, which -> dialog.cancel() }
        alertDialog.show()
    }

    fun onLocationChanged(location: SingleLocation.Companion.GPSCoordinates) {
        val latitudex = location.latitude
        val longitudex = location.longitude
        val latlng = LatLng(latitudex, longitudex)
        val venues = Venues(this)
        arrayVenue = venues.getAllMaps() as ArrayList<Venue>
        dialog(arrayVenue)

    }


    protected fun startLocationUpdates() {

        val mLocationRequest = LocationRequest()
        //mLocationRequest.priority = LocationRequest.PRIORITY_LOW_POWER
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()
        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        SingleLocation.requestSingleUpdate(this, object : SingleLocation.LocationCallback {
            override fun onNewLocationAvailable(location: SingleLocation.Companion.GPSCoordinates) {
                onLocationChanged(location)
            }

        })
    }


    fun dialog(arrayVenue: ArrayList<Venue>) {
        click =1
        val dialogs = LayoutInflater.from(this).inflate(R.layout.dialog_venue, null)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val categoryAdapter = VenueAdapter(R.layout.list_venue, arrayVenue)
        dialogs.recycler_venue.layoutManager = layoutManager
        dialogs.recycler_venue.hasFixedSize()
        dialogs.recycler_venue.adapter = categoryAdapter
        var alertbox = android.app.AlertDialog.Builder(this).apply {
            setView(dialogs)
            setNegativeButton("Back") { dialog, whichButton ->
                dialog.dismiss()
            }
            show()
        }
    }



    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.notif, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_notif -> {
                val intent = Intent(this@MainActivityDrawer, NotificationActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_scan -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Attendance")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "attendances")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_agenda -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Schedules")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "schedules")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_joining -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Instructions")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "slider")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_upload -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Moments")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "galleries")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_profile -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Profile")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "profile")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_leaderboard -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Leaderboard")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "leaderboard")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_polling -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Pollings")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "pollings")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }

            R.id.nav_teaser -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Teaser")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "teasers")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_testinonial -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Testimonials")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "testimonials")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_contact -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Contacts")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "contacts")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_venue -> {
                arrayVenue.clear()
                onLocation()
            }

            R.id.nav_feedback -> {
                SharedData.setKeyString(this@MainActivityDrawer, "title", "Feedbacks")
                SharedData.setKeyString(this@MainActivityDrawer, "embed_url", "feedbacks")
                val intent = Intent(this@MainActivityDrawer, NewActivity::class.java)
                startActivity(intent)
            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
