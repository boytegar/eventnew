package id.pentacode.cdchampionfest

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.pentacode.cdchampionfest.Service.MyService
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import id.pentacode.cdchampionfest.Adapter.ItemAdapter
import id.pentacode.cdchampionfest.Helper.SharedData
import id.pentacode.cdchampionfest.Model.Game
import org.json.JSONObject
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    var array = ArrayList<Game>()
    var act: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
            // setSupportActionBar(toolbar)
        title = "Home"


        btn_schedule.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Schedules")
            SharedData.setKeyString(this@MainActivity, "embed_url", "schedules")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_presence.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Attendance")
            SharedData.setKeyString(this@MainActivity, "embed_url", "attendances")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_feedback.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Feedbacks")
            SharedData.setKeyString(this@MainActivity, "embed_url", "feedbacks")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_contact.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Contacts")
            SharedData.setKeyString(this@MainActivity, "embed_url", "contacts")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }

        btn_gallery.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Galleries")
            SharedData.setKeyString(this@MainActivity, "embed_url", "galleries")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_polling.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Pollings")
            SharedData.setKeyString(this@MainActivity, "embed_url", "pollings")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_qna.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Questions")
            SharedData.setKeyString(this@MainActivity, "embed_url", "questions")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_quiz.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Quizes")
            SharedData.setKeyString(this@MainActivity, "embed_url", "quizes")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_test.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Testimonials")
            SharedData.setKeyString(this@MainActivity, "embed_url", "testimonials")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }
        btn_profile.setOnClickListener {
            SharedData.setKeyString(this@MainActivity,"title","Profile")
            SharedData.setKeyString(this@MainActivity, "embed_url", "profile")
            val intent = Intent(this@MainActivity, NewActivity::class.java)
            startActivity(intent)

        }


        //loads()
        val intents = Intent(this@MainActivity, MyService::class.java)
        startService(intents)
    }

//    fun loads() {
//        val server = SharedData.getKeyString(this@MainActivity, "url_api")
//        val param = listOf("user_id" to 1)
//        Fuel.get(server + "games", param).response { request, response, result ->
//            when (result) {
//                is Result.Failure -> {
//
//                }
//                is Result.Success -> {
//                    try{
//                        val data = JSONObject(String(response.data))
//                        val status = data.getJSONObject("status")
//                        val code = status.getInt("code")
//                        val message = status.getString("message")
//                        when (code) {
//                            1 -> {
//                                val games = data.getJSONArray("games")
//                                for (i in 0 until games.length()) {
//                                    val alldata = games[i] as JSONObject
//                                    val id = alldata.getInt("id")
//                                    val cover = alldata.getString("cover")
//                                    val name = alldata.getString("name")
//                                    val desc = alldata.getString("description")
//                                    val type = alldata.getString("type")
//                                    val status = alldata.getBoolean("status")
//                                    when(status){
//                                        false->{
//                                            act++
//                                        }
//                                    }
//                                    val game = Game()
//                                    game.type = type
//                                    game.id = id
//                                    game.desc = desc
//                                    game.title = name
//                                    game.image = cover
//                                    game.status = status
//                                    array.add(game)
//                                }
//                            }
//                            else -> {
//                                Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
//                            }
//                        }
//                    }
//                    catch (e: Exception){}
//                    finally {
//                        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//                        val categoryAdapter = ItemAdapter(R.layout.list_game, array)
//                        recycle_item.layoutManager = layoutManager
//                        recycle_item.hasFixedSize()
//                        recycle_item.adapter = categoryAdapter
//                        txt_activities.text = ("Today Activity : "+act.toString()+"/"+array.size.toString())
//                    }
//
//                }
//
//            }
//        }
//    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.notif, menu)

        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        if (id == R.id.action_notif) {
            val intent = Intent(this@MainActivity, NotificationActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }
}
