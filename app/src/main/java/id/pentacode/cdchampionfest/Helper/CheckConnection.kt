package id.pentacode.dfa.Helper

class CheckConnection {
    companion object {

        fun isConnected(): Boolean {
            try {
                val command = "ping -c 1 google.com"
                return Runtime.getRuntime().exec(command).waitFor() == 0
            } catch (e: Exception) {
                return false
            }

        }
    }

}