package id.pentacode.cdchampionfest.Helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class SQLiteHelper(context: Context) : SQLiteOpenHelper(context, "cd.db", null, 1) {


    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {

        sqLiteDatabase.execSQL(
            "create table tb_venue" +
                    "(id integer, name text, " +
                    "address text, " +
                    "kode text, " +
                    "latitude double, " +
                    "longitude double)"
        )

        sqLiteDatabase.execSQL(
            ("create table tb_notif" +
                    "(id integer, name text, " +
                    "description text, " +
                    "type text, " +
                    "time text, " +
                    "status integer)")
        )
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS customer")
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS product")
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS summary")
        onCreate(sqLiteDatabase)
    }
}