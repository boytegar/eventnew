package id.pentacode.cdchampionfest.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.pentacode.cdchampionfest.Model.Game
import kotlinx.android.synthetic.main.list_game.view.*
import com.bumptech.glide.Glide
import android.R
import android.content.Intent
import android.util.Log
import com.bumptech.glide.request.RequestOptions
import id.pentacode.cdchampionfest.Helper.SharedData
import id.pentacode.cdchampionfest.NewActivity
import id.pentacode.cdchampionfest.QRCodeActivity
import id.pentacode.cdchampionfest.QRGameActivity
import java.nio.file.Files.size


class ItemAdapter(var layout: Int, var list: ArrayList<Game>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    val asd = list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = asd[position]
        holder.bind(item)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(room: Game) {
            val options = RequestOptions().centerCrop()
            val url = room.cover
            Glide.with(itemView.context).load(url).apply(options).into(itemView.bg_image)
            val status = room.scan_status
            val type = room.type
            Log.e("URL LIST", room.next_url)
            itemView.setOnClickListener {
                Log.e("DIPENCET", room.next_url)
                when(status){
                    true ->{
                        when(room.webview_status){
                            true->{
                                SharedData.setKeyString(itemView.context,"embed_url", room.next_url!!)
                                val intent = Intent(itemView.context,QRGameActivity::class.java)
                                itemView.context.startActivity(intent)
                               // Log.e("DIPENCET", room.next_url)
                            }
                            else->{
                                SharedData.setKeyString(itemView.context,"embed_url", room.next_url!!)
                                val intent = Intent(itemView.context,NewActivity::class.java)
                                itemView.context.startActivity(intent)
                              //  Log.e("DIPENCET", room.next_url)
                            }
                        }
                    }
                    else->{
                        var alertbox = android.app.AlertDialog.Builder(itemView.context).apply {
                            setMessage("Anda Sudah Memaikan Game Ini")
                            setPositiveButton("OK") { dialog, whichButton ->
                                //pass
                                dialog.dismiss()
                            }

                            show()
                        }
                    }
                }
            }


        }
    }

    override fun onCreateViewHolder(views: ViewGroup, position: Int): ItemAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(views.context).inflate(layout, views, false)
        return ViewHolder(layoutInflater)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}