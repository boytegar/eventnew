package id.pentacode.cdchampionfest.Adapter


import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.pentacode.cdchampionfest.Model.Notification
import kotlinx.android.synthetic.main.list_notification.view.*


class NotificationAdapter(var layout: Int, var list: ArrayList<Notification>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    val asd = list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = asd[position]
        holder.bind(item)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(room: Notification) {
            itemView.txt_time.text = room.name
            itemView.txt_desc.text = room.desc
        }
    }

    override fun onCreateViewHolder(views: ViewGroup, position: Int): NotificationAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(views.context).inflate(layout, views, false)
        return ViewHolder(layoutInflater)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}