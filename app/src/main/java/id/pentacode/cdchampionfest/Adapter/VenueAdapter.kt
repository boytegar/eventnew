package id.pentacode.cdchampionfest.Adapter

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.maps.model.LatLng
import dmax.dialog.SpotsDialog
import id.pentacode.cdchampionfest.Model.Venue
import id.pentacode.cdchampionfest.SQLite.Venues
import id.pentacode.dfa.Helper.SingleLocation
import kotlinx.android.synthetic.main.list_venue.view.*
import javax.xml.transform.dom.DOMLocator

class VenueAdapter(var layout: Int, var list: ArrayList<Venue>) : RecyclerView.Adapter<VenueAdapter.ViewHolder>() {
    val asd = list

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = asd[position]
        holder.bind(item)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lat_dest: Double? = null
        var lng_dest: Double? = null
        val dialog: android.app.AlertDialog = SpotsDialog.Builder()
            .setContext(itemView.context)
            .setMessage("Tunggu")
            .setCancelable(false)
            .build()
        fun bind(room: Venue) {
            itemView.txt_nama.text = room.name
            itemView.txt_alamat.text = room.address
            lat_dest = room.lat
            lng_dest = room.lng

            itemView.setOnClickListener {
                //dialog.show()
                val uri = "http://maps.google.com/maps?daddr=$lat_dest,$lng_dest"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                itemView.context.startActivity(intent)
               // onLocation()
            }
        }

        fun onLocation() {
            val manager = itemView.context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showSettingsAlert()
            } else {
                SingleLocation.requestSingleUpdate(itemView.context, object : SingleLocation.LocationCallback {
                    override fun onNewLocationAvailable(location: SingleLocation.Companion.GPSCoordinates) {
                        if (location != null) {
                            onLocationChanged(location)
                        } else {
                            Toast.makeText(itemView.context, "Lokasi Sedang Di Ambil", Toast.LENGTH_SHORT).show()
                            startLocationUpdates()
                        }
                    }

                })
            }
        }

        private fun showSettingsAlert() {
            val alertDialog = AlertDialog.Builder(itemView.context)
            alertDialog.setTitle("GPS is not Enabled!")
            alertDialog.setMessage("Do you want to turn on GPS?")
            alertDialog.setPositiveButton("Yes") { dialog, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                itemView.context.startActivity(intent)
            }
            alertDialog.setNegativeButton("No") { dialog, which -> dialog.cancel() }
            alertDialog.show()
        }

        fun onLocationChanged(location: SingleLocation.Companion.GPSCoordinates) {
            val latitudex = location.latitude
            val longitudex = location.longitude
            dialog.dismiss()
            val uri = "http://maps.google.com/maps?saddr=$latitudex,$longitudex&daddr=$lat_dest,$lng_dest"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            itemView.context.startActivity(intent)

        }


        protected fun startLocationUpdates() {

            val mLocationRequest = LocationRequest()
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest)
            val locationSettingsRequest = builder.build()
            val settingsClient = LocationServices.getSettingsClient(itemView.context)
            settingsClient.checkLocationSettings(locationSettingsRequest)
            if (ActivityCompat.checkSelfPermission(
                    itemView.context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    itemView.context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            SingleLocation.requestSingleUpdate(itemView.context, object : SingleLocation.LocationCallback {
                override fun onNewLocationAvailable(location: SingleLocation.Companion.GPSCoordinates) {
                    onLocationChanged(location)
                }

            })
        }

    }

    override fun onCreateViewHolder(views: ViewGroup, position: Int): VenueAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(views.context).inflate(layout, views, false)
        return ViewHolder(layoutInflater)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}