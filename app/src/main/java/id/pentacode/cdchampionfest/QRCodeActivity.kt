package id.pentacode.cdchampionfest

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import id.pentacode.cdchampionfest.Helper.SharedData

import kotlinx.android.synthetic.main.activity_qrcode.*
import kotlinx.android.synthetic.main.content_qrcode.*
import org.json.JSONObject
import java.util.*

class QRCodeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcode)
        setSupportActionBar(toolbar)
        val formats = Arrays.asList(BarcodeFormat.QR_CODE)
        barcode_view.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        barcode_view.initializeFromIntent(intent)
        barcode_view.decodeContinuous(callback)
        barcode_view.setStatusText("Pastikan Posisi Barcode Pas")
    }
    override fun onResume() {
        super.onResume()
        barcode_view.resume()
    }

    override fun onPause() {
        super.onPause()
        barcode_view.pause()
    }


    private val callback = object : BarcodeCallback {
        var lastText: String? = null
        override fun barcodeResult(result: BarcodeResult) {
            lastText = result.text
           // barcode_view.setStatusText(result.text)
            if (result.text == null || result.text == lastText) {
                barcode_view.pause()
                //dialog(lastText.toString())
                postScan(lastText.toString())
            }
        }

        override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    }
    fun dialog(message: String) {
      //  val dialogs = LayoutInflater.from(this@DetailPengirimanActivity).inflate(R.layout.dialog_reason, null)
        var alertbox = android.app.AlertDialog.Builder(this).apply {
            setTitle(message)
            setPositiveButton("Yes") { dialog, whichButton ->
                //pass
                finish()
            }
            show()
        }
    }

    fun postScan(word: String){
        val server = SharedData.getKeyString(this@QRCodeActivity,"url_api")
        val id = SharedData.getKeyString(this@QRCodeActivity,"id_att")
        val email = SharedData.getKeyString(this@QRCodeActivity,"email_att")
        Log.e("KEYWORD", "*"+word+"*")
        val param = listOf("my_email" to email, "keyword" to word)
        Fuel.post(server!!+"attendances/"+id,param).response{ request, response, result ->
            Log.e("BALIKAN", response.toString())
            when (result) {
            is Result.Success -> {
                val data = JSONObject(String(response.data))
                val status = data.getJSONObject("status")
                val code = status.getInt("code")
                val message = status.getString("message")
                when(code){
                    1->{
                        //finish()
                        dialog(message)
                     //   Toast.makeText(this@QRCodeActivity, message, Toast.LENGTH_SHORT).show()
                    }
                    else->{
                        dialog(message)
                       // Toast.makeText(this@QRCodeActivity, message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
                is Result.Failure ->{
                    //if error
                }
            }


        }
    }

}
