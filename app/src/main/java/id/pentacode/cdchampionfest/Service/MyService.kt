package id.pentacode.cdchampionfest.Service

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import id.pentacode.cdchampionfest.Helper.SharedData
import id.pentacode.cdchampionfest.SQLite.Notifications
import id.pentacode.cdchampionfest.Model.Notification
import org.json.JSONObject
import java.lang.Exception
import java.util.*
import java.text.SimpleDateFormat
import android.app.*

import android.os.Handler
import android.os.Message
import id.pentacode.cdchampionfest.R
import id.pentacode.cdchampionfest.MainActivityDrawer
import org.jetbrains.anko.doAsync


@Suppress("DEPRECATION")
class MyService : Service() {
    private val timer = Timer()
    private var ctx: Context? = null

    override fun onBind(arg0: Intent): IBinder? {

        return null
    }

    override fun onCreate() {
        super.onCreate()
        ctx = this
        startService()
    }

    private fun startService() {
        timer.scheduleAtFixedRate(mainTask(), 0, 60000 * 5)
    }

    private inner class mainTask : TimerTask() {
        override fun run() {
            toastHandler.sendEmptyMessage(0)
        }
    }

    override fun onDestroy() {
        timer.cancel()
        super.onDestroy()
    }

    private val toastHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            doAsync {
                checkNotif()
            }
        }
    }

    fun checkNotif() {
        val notif = Notifications(this@MyService)
        val server = SharedData.getKeyString(this@MyService, "url_api")
        val email = SharedData.getKeyString(this@MyService, "email")
        val id = notif.getLastId()
        val params = listOf("my_email" to email, "last_notification_id" to id)
        Fuel.get(server + "notifications", params).response { request, response, result ->
            when (result) {
                is Result.Failure -> {
                    Log.e("ERROR SERVICE", result.error.toString())
                }
                is Result.Success -> {
                    val json = JSONObject(String(response.data))
                    val status = json.getJSONObject("status")
                    val code = status.getInt("code")
                    val notif = Notification()
                    when (code) {
                        1 -> {
                            try {
                                val data = json.getJSONArray("notifications")
                                for (i in 0 until data.length()) {
                                    val noti = data[i] as JSONObject
                                    notif.id = noti.getInt("id")
                                    notif.name = noti.getString("name")
                                    notif.desc = noti.getString("description")
                                    notif.type = noti.getString("type")
                                    notif.time = noti.getString("time")
                                    notif.status = 1
                                    val db_notif = Notifications(this@MyService)
                                    db_notif.populateData(notif)
                                }
                                //  getNotif()
                            } catch (e: Exception) {

                            } finally {
                                getNotif()
                            }
                        }
                        else -> {
                        }
                    }

                }
            }
        }
    }

    fun getNotif() {
        val sdfDate = SimpleDateFormat("dd-MM-yyyy HH:mm")//dd/MM/yyyy
        val now = Date()
        val strDate = sdfDate.format(now)
        val notif = Notifications(this@MyService)
        val status = 1
        val datas = notif.getNotifications(status)
        when {
            (datas.isNotEmpty()) -> {
        var title: String? = null
        var desc: String? = null
        var id: Int? = 0
        for (i in 0 until datas.size) {
            title = datas[i].name
            desc = datas[i].desc
            id = datas[i].id
        }
        val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "channel-01"
        val channelName = "Channel Name"
        val importance = NotificationManager.IMPORTANCE_HIGH

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                channelId, channelName, importance
            )
            notificationManager.createNotificationChannel(mChannel)
        }

        val mBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(desc)
        val mainIntent = Intent(this, MainActivityDrawer::class.java)
        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addNextIntent(mainIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(
            0,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        mBuilder.setContentIntent(resultPendingIntent)

        notificationManager.notify(1, mBuilder.build())
                notif.updateNotif(id!!)
            }
        }


    }

}

