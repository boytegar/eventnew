package id.pentacode.cdchampionfest

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import id.pentacode.cdchampionfest.Adapter.NotificationAdapter
import id.pentacode.cdchampionfest.Model.Notif
import id.pentacode.cdchampionfest.Model.Notification
import id.pentacode.cdchampionfest.SQLite.Notifications

import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.content_notification.*

class NotificationActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        setSupportActionBar(toolbar)
        title = "Notification"
        var array = ArrayList<Notification>()
        val notif = Notifications(this@NotificationActivity)
        val data = notif.getNotif()
        for(i in 0 until data.size){
            val isd = data[i]
            array.add(isd)
        }


        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val categoryAdapter = NotificationAdapter(R.layout.list_notification, array)
        recycle_notification.layoutManager = layoutManager
        recycle_notification.hasFixedSize()
        recycle_notification.adapter = categoryAdapter
    }

}
